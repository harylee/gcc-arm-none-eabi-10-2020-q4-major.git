# gcc-arm-none-eabi-10-2020-q4-major

#### 介绍
arm交叉编译器，用于编译stm相关单板。

#### 安装教程

1.  git clone git@gitee.com:harylee/gcc-arm-none-eabi-10-2020-q4-major.git
2.  将export PATH=~/xxxxx/gcc-arm-none-eabi-10-2020-q4-major/bin:$PATH 写入 ~/.bashrc，其中xxxxx改为实际路径。
